<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp"></jsp:include>
    <div class="container">
        <div class="row">
            <div class="input-field col s10 m10 l10">
                <i class="material-icons prefix">person</i>
                <input type="text" id="autoBusca" class="validate">
                <label for="autoBusca">Busca nome ou e-mail</label>
            </div>
            <div class="col s2 m2 l2">
                    <span id="listedTotal"></span> / <span id="allTotal"></span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                    <ul class="collection">
	                    <jsp:useBean id="dao" class="br.com.agenda.dao.ContatoSQLDAO"/>
	                    <c:forEach var="contato" items="${dao.loadAll()}">
							<li id="idContato${contato.id}" class="collection-item avatar">
	                            <i class="material-icons circle">person</i>
	                            <span class="title">${contato.nome} - ${contato.email}</span>
	                            <p class="grey-text text-lighten-2"><span class="grey-text text-darken-3">${contato.fixo}</span> # <span class="grey-text text-darken-3">${contato.ramal}</span></p>
	                            <a href="#modalUpdate" data-id="${contato.id}" class="modal-trigger third-content indigo-text text-darken-2"><i class="material-icons">edit</i></a>
	                            <a href="#modalDelete" data-id="${contato.id}" class="modal-trigger secondary-content indigo-text text-darken-2"><i class="material-icons">delete</i></a>
	                        </li>
                        </c:forEach>
                        <li id="alone" class="collection-item avatar"> Sem Resultados </li>
                    </ul>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn horizontal">
    	<a href="#modalCreate" class="btn-floating btn-large modal-trigger waves-effect waves-light indigo darken-2"><i class="material-icons">add</i></a>    
    </div>
    <jsp:include page="../modals/contatos/create.jsp"></jsp:include>
    <jsp:include page="../modals/contatos/update.jsp"></jsp:include>
    <jsp:include page="../modals/contatos/delete.jsp"></jsp:include>
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src="assets/js/contatos.js"></script>
<jsp:include page="../footer.jsp"></jsp:include>