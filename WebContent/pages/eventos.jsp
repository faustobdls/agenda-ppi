<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp"></jsp:include>
    <!--link rel="stylesheet" href="assets/css/bootstrap.min.css"-->
    <div class="container" style="padding-top: 15px;">
        <div class="row">
            <div class="col s12 m12 l12">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    <jsp:include page="../modals/eventos/details.jsp"></jsp:include>
    <jsp:include page="../modals/eventos/create.jsp"></jsp:include>
    <jsp:include page="../modals/eventos/update.jsp"></jsp:include>
    <jsp:include page="../modals/eventos/delete.jsp"></jsp:include>
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src='assets/js/moment.min.js'></script>
    <script src='assets/js/fullcalendar.js'></script>
    <script src="assets/js/eventos.js"></script>
<jsp:include page="../footer.jsp"></jsp:include>