$(document).ready(function(){
	$('select').material_select();
    Materialize.updateTextFields();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Hoje',
        clear: 'Limpar',
        close: 'Concluir',
        closeOnSelect: false // Close upon selecting a date,
    });
    $('#modalDetails').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
          console.log(modal, trigger);
        },
        complete: function() { 
            document.getElementById("participantesEvent").innerText = "";
            console.log('Closed Create'); 
        } // Callback for Modal close
      }
    );
    $('#modalCreate').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
          console.log(modal, trigger);
        },
        complete: function() { console.log('Closed Create'); } // Callback for Modal close
      }
    );
    $('#modalUpdate').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
            /*$.getJSON("/agenda/contatos?id="+trigger[0].dataset.id, {}, (res) => {
                document.getElementById("update_id").setAttribute("value", res.id);
                $("#update_id_view").html(res.id);
                $("#update_name_view").html(res.nome);
                document.getElementById("update_nome").setAttribute("value", res.nome);
                document.getElementById("update_email").setAttribute("value", res.email);
                document.getElementById("update_telefone").setAttribute("value", res.telefone);
                document.getElementById("update_fixo").setAttribute("value", res.fixo);
                document.getElementById("update_ramal").setAttribute("value", res.ramal);
                
                Materialize.updateTextFields();
            });*/
          console.log(modal, trigger);
        },
        complete: function() { 
            console.log('Closed Create'); 
            document.getElementById("update_id").setAttribute("value", "");
            $("#update_id_view").html("0");
            $("#update_name_view").html("Contato");
            /*document.getElementById("update_nome").setAttribute("value", "");
            document.getElementById("update_email").setAttribute("value", "");
            document.getElementById("update_telefone").setAttribute("value", "");
            document.getElementById("update_fixo").setAttribute("value", "");
            document.getElementById("update_ramal").setAttribute("value", "");*/
            Materialize.updateTextFields();
        } // Callback for Modal close
      }
    );
    $('#modalDelete').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
            document.getElementById("deleteId").setAttribute("value", trigger[0].dataset.id);
            var btn = document.getElementById("btnYesDelete");
            btn.addEventListener('click', () => {
                document.getElementById("formDelete").submit()
                $('#modalDelete').modal('close');
            });
          console.log(modal, trigger);
        },
        complete: function() { 
            document.getElementById("deleteId").setAttribute("value","0");
            console.log('Closed Create'); 
        } // Callback for Modal
      }
    );
    $('#calendar').fullCalendar({
        //themeSystem: 'bootstrap3',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        //defaultDate: '2017-10-12',
        weekNumbers: false,
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: function(start, end, timezone, callback) {
            $.ajax({
                url: '/agenda/eventos?list=full',
                dataType: 'json',
                success: function(doc) {
                    var events = [];
                    doc.forEach(function(el, i) {
                        var date = el.data.split(" ");
                        var months = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun','Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
                        for(var j=0;j<months.length;j++){
                            if(date[0] == months[j].toLocaleLowerCase()){
                                 date[0]=j+1;
                             }                      
                        } 
                        if(date[0]<10){
                            date[0] = '0'+date[0];
                        }                
                        var formattedDate = date[2] + '-' + date[0] + '-' + date[1].split(",")[0];
                        events.push({id: el.id, title: el.nome, start: formattedDate});
                    });
                    callback(events);
                }
            });
        },
        dayClick: (date, jsEvent, view, obj) => {
            console.log(date.format());
            document.getElementById("data").setAttribute("value", date.format());
            Materialize.updateTextFields();
            $('#modalCreate').modal('open');
        },
        eventClick: function(calEvent, jsEvent, view) {
            $.getJSON("/agenda/eventos", {id: calEvent.id}, (response) => {
                $("#titleEvent").html(response.nome);
                $("#descricaoEvent").html(response.descricao);
                response.participantes.forEach(function(el, i) {
                    let li = document.createElement('li');
                    li.appendChild(document.createTextNode(`${el.nome} - ${el.email} # ${el.telefone}`))
                    document.getElementById("participantesEvent").appendChild(li);
                });
                document.getElementById("loadModalUpdate").setAttribute("data-id", response.id);
                document.getElementById("loadModalDelete").setAttribute("data-id", response.id);
                $("#modalDetails").modal('open');
            });
            console.log(calEvent.id, view.name, jsEvent);
        }
    })
});

    