$(document).ready(function(){
    Materialize.updateTextFields();
    $('#modalCreate').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
          console.log(modal, trigger);
        },
        complete: function() { console.log('Closed Create'); } // Callback for Modal close
      }
    );
    $('#modalUpdate').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
            $.getJSON("/agenda/contatos?id="+trigger[0].dataset.id, {}, (res) => {
                document.getElementById("update_id").setAttribute("value", res.id);
                $("#update_id_view").html(res.id);
                $("#update_name_view").html(res.nome);
                document.getElementById("update_nome").setAttribute("value", res.nome);
                document.getElementById("update_email").setAttribute("value", res.email);
                document.getElementById("update_telefone").setAttribute("value", res.telefone);
                document.getElementById("update_fixo").setAttribute("value", res.fixo);
                document.getElementById("update_ramal").setAttribute("value", res.ramal);
                
                Materialize.updateTextFields();
            });
          console.log(modal, trigger);
        },
        complete: function() { 
            console.log('Closed Create'); 
            document.getElementById("update_id").setAttribute("value", "");
            $("#update_id_view").html("0");
            $("#update_name_view").html("Contato");
            document.getElementById("update_nome").setAttribute("value", "");
            document.getElementById("update_email").setAttribute("value", "");
            document.getElementById("update_telefone").setAttribute("value", "");
            document.getElementById("update_fixo").setAttribute("value", "");
            document.getElementById("update_ramal").setAttribute("value", "");
            Materialize.updateTextFields();
        } // Callback for Modal close
      }
    );
    $('#modalDelete').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            console.log("Ready Create");
            document.getElementById("deleteId").setAttribute("value", trigger[0].dataset.id);
            var btn = document.getElementById("btnYesDelete");
            btn.addEventListener('click', () => {
                document.getElementById("formDelete").submit()
                $('#modalDelete').modal('close');
            });
          console.log(modal, trigger);
        },
        complete: function() { 
            document.getElementById("deleteId").setAttribute("value","0");
            console.log('Closed Create'); 
        } // Callback for Modal close
      }
    );
    $.getJSON("/agenda/contatos?list=full", {}, (response) => {
        $("#allTotal").html("Total(" + response.length + ")");
        $("#listedTotal").html("Exibido(" + response.length + ")");
    });
    $(".collection-item").show();
    $("#alone").hide();
    document.getElementById("autoBusca").addEventListener("keyup", (e) => {
        
        $.getJSON("/agenda/contatos?q=" + e.target.value, {}, (response) => {
            $("#listedTotal").html("Exibido(" + response.length + ")");
            //console.log(response);
            $(".collection-item").hide();
            response.forEach((e, i) => {
                $("#idContato" + e.id).show();
            });
            if(response.length == 0){
                $("#alone").show();
            }else{
                $("#alone").hide();
            }
        });
    });
  });