<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agenda PPI</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel='stylesheet' href='assets/css/fullcalendar.css' />
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="navbar-fixed">
        <nav class="indigo darken-2">
            <div class="nav-wrapper">
                <a href="/agenda" class="brand-logo center">Agenda PPI</a>
            </div>
        </nav>
    </div>