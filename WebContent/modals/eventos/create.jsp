<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Modal Create -->
<div id="modalCreate" class="modal">
    <div class="modal-content">
        <h4>Adicionar Evento</h4>
        <form id="formCreate" method="POST" action="/agenda/eventos">
            <input type="hidden" name="method" value="post">
            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">person</i>
                    <input id="nome" name="nome" type="text" class="validate">
                    <label class="active" for="nome">Nome</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">textsms</i>
                    <textarea id="descricao" name="descricao" class="validate"></textarea>
                    <label class="active" for="descricao">Descricao</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">date_range</i>
                    <input id="data" name="data" type="text" class="datepicker">
                    <label class="active" for="data">Data do Evento</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select name="participantes" multiple>
                        <option value="0" disabled selected>Escolha os participantes</option>
                        <jsp:useBean id="dao" class="br.com.agenda.dao.ContatoSQLDAO"/>
                        <c:forEach var="contato" items="${dao.loadAll()}">
                            <option value="${contato.id}">${contato.nome}</option>
                        </c:forEach>
                    </select>
                    <label>Participantes</label>
                  </div>
                <div class="col s12 m12 l12">
                    <button class="waves-effect waves-green btn btn-block" type="submit">Adicionar</button>
                </div>
            </div>
        </form>
    </div>
</div>