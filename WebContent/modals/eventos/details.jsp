<!-- Modal Detail -->
<div id="modalDetails" class="modal">
    <div class="modal-content">
        <h4 id="titleEvent"></h4>
        <div class="col s12 m12 l12">
            <p id="descricaoEvent"></p>
        </div>
        <div class="col s12 m12 l12">
            <p id="descricaoEvent"></p>
        </div>
        <div class="col s12 m12 l12">
            <h5>Participantes do evento</h5>
            <ul id="participantesEvent"></ul>
        </div>
    </div>
    <div class="modal-footer">
        <a id="loadModalUpdate" href="#modalUpdate" data-id="0" class="modal-action modal-trigger modal-close waves-effect waves-red btn">Atualizar</a>
        <a id="loadModalDelete" href="#modalDelete" data-id="0" class="modal-action modal-trigger modal-close waves-effect waves-green btn">Deletar</a>
    </div>
</div>