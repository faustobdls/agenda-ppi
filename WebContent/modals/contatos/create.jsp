<!-- Modal Create -->
<div id="modalCreate" class="modal">
    <div class="modal-content">
        <h4>Adicionar Contato</h4>
        <form id="formCreate" method="POST" action="/agenda/contatos">
            <input type="hidden" name="method" value="post">
            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">person</i>
                    <input id="nome" name="nome" type="text" class="validate">
                    <label class="active" for="nome">Nome</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">email</i>
                    <input id="email" name="email" type="text" class="validate">
                    <label class="active" for="email">Email</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">phone</i>
                    <input id="telefone" name="telefone" type="text" class="validate">
                    <label class="active" for="telefone">Telefone</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">phone</i>
                    <input id="fixo" name="fixo" type="text" class="validate">
                    <label class="active" for="fixo">Fixo</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">phone</i>
                    <input id="ramal" name="ramal" type="text" class="validate">
                    <label class="active" for="ramal">Ramal</label>
                </div>
                <div class="col s12 m12 l12">
                    <button class="waves-effect waves-green btn btn-block" type="submit">Adicionar</button>
                </div>
            </div>
        </form>
    </div>
</div>