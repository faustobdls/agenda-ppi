<!-- Modal Deletes -->
<div id="modalDelete" class="modal">
    <div class="modal-content">
        Realmente deseja deletar esse contato?
        <form id="formDelete" action="/agenda/contatos" method="POST">
            <input type="hidden" value="delete" name="method">
            <input type="hidden" value="0" name="id" id="deleteId">
        </form>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red btn">NÃO</a>
        <a href="#!" id="btnYesDelete" class="modal-action waves-effect waves-green btn">SIM</a>
    </div>
</div>