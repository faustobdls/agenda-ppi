<!-- Modal Update -->
<div id="modalUpdate" class="modal">
    <div class="modal-content">
        <h4>Editar <span id="update_name_view">Contato</span> # <span id="update_id_view" class="grey">Contato</span></h4>
        <form id="formUpdate" method="POST" action="/agenda/contatos">
            <input type="hidden" name="method" value="put">
            <div class="row">
                <input type="hidden" id="update_id" name="id">
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">person</i>
                    <input id="update_nome" name="nome" type="text" class="validate">
                    <label class="active" for="update_nome">Nome</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">email</i>
                    <input id="update_email" name="email" type="text" class="validate">
                    <label class="active" for="update_email">Email</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <i class="material-icons prefix">phone</i>
                    <input id="update_telefone" name="telefone" type="text" class="validate">
                    <label class="active" for="update_telefone">Telefone</label>
                </div>
                <div class="input-field col s6 m6 l6">
                    <i class="material-icons prefix">phone</i>
                    <input id="update_fixo" name="fixo" type="text" class="validate">
                    <label class="active" for="update_fixo">Fixo</label>
                </div>
                <div class="input-field col s6 m6 l6">
                    <i class="material-icons prefix">phone</i>
                    <input id="update_ramal" name="ramal" type="text" class="validate">
                    <label class="active" for="update_ramal">Ramal</label>
                </div>
                <div class="col s12 m12 l12">
                    <button class="waves-effect waves-green btn btn-block" type="submit">Adicionar</button>
                </div>
            </div>
        </form>
    </div>
</div>