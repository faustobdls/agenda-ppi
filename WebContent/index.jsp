<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
    <div class="row">
        <div class="col s6 m6 l6">
            <div class="card">
                <div class="card-image">
                    <img src="http://via.placeholder.com/350x150?text=Contatos">
                    <span class="card-title">Contatos</span>
                    <a href="contatos" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">send</i></a>
                </div>
            </div>
        </div>
        <div class="col s6 m6 l6">
                <div class="card">
                    <div class="card-image">
                        <img src="http://via.placeholder.com/350x150?text=Eventos">
                        <span class="card-title">Eventos</span>
                        <a href="eventos" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">send</i></a>
                    </div>
                </div>
            </div>
    </div>
<jsp:include page="footer.jsp"></jsp:include>