package br.com.agenda.connection;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.Pragma;


public class ConnectionFactory {
	private ConnectionFactory(){}
	public static Connection getConnection() {
		try {
			DriverManager.registerDriver(new org.sqlite.JDBC());
			Class.forName("org.sqlite.JDBC");
			Path db = Paths.get("Documentos/agenda-ppi/src/br/com/agenda/data/agenda.db");
			//System.out.println(db.toAbsolutePath());
			SQLiteConfig sqLiteConfig = new SQLiteConfig();
			Properties properties = sqLiteConfig.toProperties();
			properties.setProperty(Pragma.DATE_STRING_FORMAT.pragmaName, "yyyy-MM-dd HH:mm:ss");
			return DriverManager.getConnection("jdbc:sqlite:" + db.toAbsolutePath(), properties);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
