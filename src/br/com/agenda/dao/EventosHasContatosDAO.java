package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.agenda.connection.ConnectionFactory;
import br.com.agenda.model.Contato;
import br.com.agenda.model.Evento;

public class EventosHasContatosDAO {
	public List<Contato> getContatosByEvent(Evento event){
		String sql = "SELECT * FROM Contatos WHERE Contatos.id IN (SELECT contatos_id FROM contatos_has_eventos WHERE eventos_id=" + event.getId() + ")";
		ArrayList<Contato> cats = new ArrayList<Contato>();
        try (Connection conn = ConnectionFactory.getConnection();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            while (rs.next()) {
            	cats.add(
            			new Contato(
            					rs.getInt("id"), 
            					rs.getString("nome"),
            					rs.getString("email"),
            					rs.getString("telefone"),
            					rs.getString("fixo"),
            					rs.getString("ramal")
            					)
            			);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return cats;
	}
	
	public void setContatosInEvent(List<Contato> con, Evento event){
		String resultSQL = "INSERT INTO contatos_has_eventos(contatos_id, eventos_id) VALUES (?,?);";
		
		con.forEach( contato -> {
			try (Connection conn = ConnectionFactory.getConnection();
		            PreparedStatement pstmt = conn.prepareStatement(resultSQL)) {
				pstmt.setInt(1, contato.getId());
				pstmt.setInt(2, event.getId());
					pstmt.executeUpdate();
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
		} );
		
	}
	
}
