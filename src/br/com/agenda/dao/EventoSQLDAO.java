package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.agenda.connection.ConnectionFactory;
import br.com.agenda.model.Evento;

public class EventoSQLDAO implements EventoDAO {

	@Override
	public ArrayList<Evento> loadAll() {
		String sql = "SELECT * FROM Eventos";
		ArrayList<Evento> events = new ArrayList<Evento>();
		try (Connection conn = ConnectionFactory.getConnection();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql)){

	            while (rs.next()) {
	            	Evento event = new Evento(
        					rs.getInt("id"), 
        					rs.getString("nome"),
        					rs.getString("descricao"),
        					rs.getDate("data")
        					);
	            	event.setParticipantes(DAOFactory.getEhCDAO().getContatosByEvent(event));
	            	events.add( event );
	            }
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		return events;
	}

	@Override
	public Evento loadOne(int id) {
		String sql = "SELECT * FROM Eventos WHERE id=" + id;
		ArrayList<Evento> events = new ArrayList<Evento>();
		try (Connection conn = ConnectionFactory.getConnection();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql)){

				while (rs.next()) {
	            	Evento event = new Evento(
	    					rs.getInt("id"), 
	    					rs.getString("nome"),
	    					rs.getString("descricao"),
	    					rs.getDate("data")
	    					);
	            	event.setParticipantes(DAOFactory.getEhCDAO().getContatosByEvent(event));
	            	events.add( event );
	            }
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		if(events.isEmpty()){
			return new Evento();
		}else{
			return events.get(0);
		}
	}

	@Override
	public void store(Evento item) {
		String sql = "INSERT INTO Eventos(nome,descricao,data) VALUES(?,?,?)";
		 
        try (Connection conn = ConnectionFactory.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
	            pstmt.setString(1, item.getNome());
	            pstmt.setString(2, item.getDescricao());
	            pstmt.setDate(3, new java.sql.Date(item.getData().getTime()));
	            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

	@Override
	public List<Evento> filter(String filter) {
		return this.loadAll().stream().filter(
				e -> 	e.getNome().toUpperCase().equals(filter.toUpperCase()) || 
						e.getDescricao().toUpperCase().equals(filter.toUpperCase())
				).collect(Collectors.toList());
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<Evento> filter(int y, int m, int d) {
		System.out.println(d + "/" + m + "/" + y);
		return this.loadAll().stream().filter(
				e -> 	e.getData().getDate() == d ||
						e.getData().getMonth() == m ||
						e.getData().getYear() == y
				).collect(Collectors.toList());
	}

	@Override
	public void update(Evento item) {
		String sql = "UPDATE Eventos SET nome = ? ,descricao = ? ,data= ? WHERE id = ?";
		 
        try (Connection conn = ConnectionFactory.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
        	pstmt.setString(1, item.getNome());
            pstmt.setString(2, item.getDescricao());
            pstmt.setDate(3, new java.sql.Date(item.getData().getTime()));
            pstmt.setInt(4, item.getId());
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

	@Override
	public void delete(int id) {
		String sql = "DELETE FROM Eventos WHERE id = ?";
		 
        try (Connection conn = ConnectionFactory.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
             pstmt.setInt(1, id);
             System.out.println("DAO: " + id);
            pstmt.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

}
