package br.com.agenda.dao;
import java.util.List;

import br.com.agenda.model.Evento;

public interface EventoDAO extends DAO<Evento> {

	List<Evento> filter(int y, int m, int d);

}
