package br.com.agenda.dao;

public class DAOFactory {
	
	private DAOFactory(){}

	public static ContatoDAO getContatoDAO(){
		return new ContatoSQLDAO();
	}
	
	public static EventoDAO getEventoDAO(){
		return new EventoSQLDAO();
	}
	
	public static EventosHasContatosDAO getEhCDAO(){
		return new EventosHasContatosDAO();
	}
}
