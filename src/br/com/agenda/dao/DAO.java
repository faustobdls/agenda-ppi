package br.com.agenda.dao;

import java.util.List;

public interface DAO<T> {
	
	public List<T> loadAll();
	
	public T loadOne(int id);
	
	public void store(T item);
	
	public List<T> filter(String filter);
	
	public void update(T item);
	
	public void delete(int id);
	
}
