package br.com.agenda.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.agenda.connection.ConnectionFactory;
import br.com.agenda.model.Contato;
import br.com.agenda.model.Evento;

public class ContatoSQLDAO implements ContatoDAO{

	@Override
	public List<Contato> loadAll() {
		String sql = "SELECT * FROM Contatos";
		List<Contato> cats = new ArrayList<Contato>();
        try (Connection conn = ConnectionFactory.getConnection();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            while (rs.next()) {
            	cats.add(
            			new Contato(
            					rs.getInt("id"), 
            					rs.getString("nome"),
            					rs.getString("email"),
            					rs.getString("telefone"),
            					rs.getString("fixo"),
            					rs.getString("ramal")
            					)
            			);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return cats;
	}

	@Override
	public Contato loadOne(int id) {
		String sql = "SELECT * FROM Contatos WHERE id=" + id;
		ArrayList<Contato> cats = new ArrayList<Contato>();
        try (Connection conn = ConnectionFactory.getConnection();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

        	while (rs.next()) {
            	cats.add(
            			new Contato(
            					rs.getInt("id"), 
            					rs.getString("nome"),
            					rs.getString("email"),
            					rs.getString("telefone"),
            					rs.getString("fixo"),
            					rs.getString("ramal")
            					)
            			);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if(cats.isEmpty()){
			return new Contato();
		}else{
			return cats.get(0);
		}
	}

	@Override
	public void store(Contato item) {
		String sql = "INSERT INTO Contatos(nome,email,telefone,fixo,ramal) VALUES(?,?,?,?,?)";
		 
        try (Connection conn = ConnectionFactory.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, item.getNome());
            pstmt.setString(2, item.getEmail());
            pstmt.setString(3, item.getTelefone());
            pstmt.setString(4, item.getFixo());
            pstmt.setString(5, item.getRamal());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

	@Override
	public List<Contato> filter(String filter) {
		String text = filter.toUpperCase();
		return this.loadAll().stream().filter(
				c -> String.valueOf( c.getNome().toUpperCase() ).contains(text) || 
				c.getEmail().toUpperCase().contains(text)
				).collect(Collectors.toList());
	}

	@Override
	public void update(Contato item) {
		String sql = "UPDATE Contatos SET nome = ? ,email = ? ,telefone = ? ,fixo = ? ,ramal = ? WHERE id = ?";
 
        try (Connection conn = ConnectionFactory.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
        	pstmt.setString(1, item.getNome());
            pstmt.setString(2, item.getEmail());
            pstmt.setString(3, item.getTelefone());
            pstmt.setString(4, item.getFixo());
            pstmt.setString(5, item.getRamal());
            pstmt.setInt(6, item.getId());
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

	@Override
	public void delete(int id) {
		String sql = "DELETE FROM Contatos WHERE id = ?";
		 
        try (Connection conn = ConnectionFactory.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
             pstmt.setInt(1, id);
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}

}
