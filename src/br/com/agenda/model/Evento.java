package br.com.agenda.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "evento")
public class Evento {

	private int id;
	private String nome;
	private String descricao;
	private Date data;
	private List<Contato> participantes;
	
	public Evento(){}

	public Evento(int id, String nome, String descricao, Date data) {
		super();
		setId(id);
		setNome(nome);
		setDescricao(descricao);
		setData(data);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<Contato> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<Contato> participantes) {
		this.participantes = participantes;
	}

	public void addContato(Contato c){
		this.participantes.add(c);
	}
	
	public void removeContato(Contato c){
		this.participantes.remove(c);
	}
	
}
