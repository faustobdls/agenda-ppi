package br.com.agenda.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "contato")
public class Contato {
	private int id;
	private String nome;
	private String email;
	private String telefone;
	private String fixo;
	private String ramal;

	public Contato(){}
	
	public Contato(int id, String nome, String email, String telefone, String fixo, String ramal) {
		super();
		this.setId(id);
		this.setNome(nome);
		this.setEmail(email);
		this.setTelefone(telefone);
		this.setFixo(fixo);
		this.setRamal(ramal);
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getFixo() {
		return fixo;
	}

	public void setFixo(String fixo) {
		this.fixo = fixo;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
}
