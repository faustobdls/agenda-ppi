package br.com.agenda.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.agenda.MyApp;
import br.com.agenda.dao.DAOFactory;
import br.com.agenda.model.Contato;

@Path("/{mediaType}/contato")
public class ContatoResource {
	
	@GET
	@Path("/list")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getAll(@PathParam("mediaType") String mediaType){
		GenericEntity<List<Contato>> entity = new GenericEntity<List<Contato>>(DAOFactory.getContatoDAO().loadAll()){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getOne(@PathParam("mediaType") String mediaType, @PathParam("id") int id){
		GenericEntity<Contato> entity = new GenericEntity<Contato>(DAOFactory.getContatoDAO().loadOne(id)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response create(@PathParam("mediaType") String mediaType, Contato contato){
		DAOFactory.getContatoDAO().store(contato);
		GenericEntity<Contato> entity = new GenericEntity<Contato>(DAOFactory.getContatoDAO().filter(contato.getNome()).get(0)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response update(@PathParam("mediaType") String mediaType, Contato contato){
		DAOFactory.getContatoDAO().update(contato);
		GenericEntity<Contato> entity = new GenericEntity<Contato>(DAOFactory.getContatoDAO().filter(contato.getNome()).get(0)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response delete(@PathParam("mediaType") String mediaType, @PathParam("id") int id){
		GenericEntity<Contato> entity = new GenericEntity<Contato>(DAOFactory.getContatoDAO().loadOne(id)){};
		DAOFactory.getContatoDAO().delete(id);
		return MyApp.mountResponse(entity, mediaType);
	}
	
}
