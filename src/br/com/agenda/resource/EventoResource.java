package br.com.agenda.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.agenda.MyApp;
import br.com.agenda.dao.DAOFactory;
import br.com.agenda.model.Evento;

@Path("/{mediaType}/evento")
public class EventoResource {
	
	@GET
	@Path("/list")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getAll(@PathParam("mediaType") String mediaType){
		GenericEntity<List<Evento>> entity = new GenericEntity<List<Evento>>(DAOFactory.getEventoDAO().loadAll()){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@GET
	@Path("/bydate/{day}/{month}/{year}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getAllByDate(@PathParam("mediaType") String mediaType, @PathParam("day") int d,@PathParam("month") int m,@PathParam("year") int y){
		GenericEntity<List<Evento>> entity = new GenericEntity<List<Evento>>(DAOFactory.getEventoDAO().filter(y, m, d)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getAll(@PathParam("mediaType") String mediaType, @PathParam("id") int id){
		GenericEntity<Evento> entity = new GenericEntity<Evento>(DAOFactory.getEventoDAO().loadOne(id)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response create(@PathParam("mediaType") String mediaType, Evento evento){
		DAOFactory.getEventoDAO().store(evento);
        DAOFactory.getEhCDAO().setContatosInEvent(evento.getParticipantes(), DAOFactory.getEventoDAO().filter(evento.getNome()).get(0));
		GenericEntity<Evento> entity = new GenericEntity<Evento>(DAOFactory.getEventoDAO().filter(evento.getNome()).get(0)){};
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response update(@PathParam("mediaType") String mediaType, Evento evento){
		DAOFactory.getEventoDAO().update(evento);
		DAOFactory.getEhCDAO().setContatosInEvent(evento.getParticipantes(), DAOFactory.getEventoDAO().filter(evento.getNome()).get(0));
		GenericEntity<Evento> entity = new GenericEntity<Evento>(DAOFactory.getEventoDAO().filter(evento.getNome()).get(0)){}; 
		return MyApp.mountResponse(entity, mediaType);
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response delete(@PathParam("mediaType") String mediaType, @PathParam("id") int id){
		GenericEntity<Evento> entity = new GenericEntity<Evento>(DAOFactory.getEventoDAO().loadOne(id)){};
		DAOFactory.getEventoDAO().delete(id);
		return MyApp.mountResponse(entity, mediaType);
	}
	
}
