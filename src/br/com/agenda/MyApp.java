package br.com.agenda;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.jettison.JettisonFeature;

public class MyApp  extends Application {
	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();
		singletons.add(new JettisonFeature());
		return singletons;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<>();
		properties.put("jersey.config.server.provider.packages", "br.com.agenda.resource");
		return properties;
	}

	public static String selectType(String string){
		if(string.equals("xml")){
			return MediaType.APPLICATION_XML;
		}else{
			return MediaType.APPLICATION_JSON;
		}
	}
	
	public static <T> Response mountResponse(GenericEntity<T> entity, String mediaType) {
		return Response
				.ok(entity)
				.header(HttpHeaders.CONTENT_TYPE, MyApp.selectType(mediaType))
				.build();
	}
}
