package br.com.agenda.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import br.com.agenda.dao.DAOFactory;
import br.com.agenda.model.Contato;

/**
 * Servlet implementation class ContatoList
 */
@WebServlet("/contatos")
public class ContatoList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContatoList() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("aplication/json");
		if(request.getParameter("id") == null || request.getParameter("id").isEmpty()){
			if(request.getParameter("q") == null || request.getParameter("q").isEmpty()){
				if(request.getParameter("list") == null || request.getParameter("list").isEmpty()){
					request.getRequestDispatcher("pages/contatos.jsp").forward(request, response);
				}else{
					Gson gson = new Gson();
					response.getWriter().print(
							gson.toJson(
									DAOFactory.getContatoDAO().loadAll()
							)
					);
				}
			}else{
				Gson gson = new Gson();
				response.getWriter().print(
						gson.toJson(
								DAOFactory.getContatoDAO().filter(
										request.getParameter("q")
								)
						)
				);
				
			}
		}else{
			Gson gson = new Gson();
			response.getWriter().print(
					gson.toJson(
							DAOFactory.getContatoDAO().loadOne(
									Integer.parseInt(
											request.getParameter("id")
									)
							)
					)
			);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		switch (req.getParameter("method")) {
		case "post":
				DAOFactory.getContatoDAO().store(
						new Contato(
								0,
								req.getParameter("nome"),
								req.getParameter("email"),
								req.getParameter("telefone"),
								req.getParameter("fixo"),
								req.getParameter("ramal")
						)
				);
				req.setAttribute("message", "Contato Criado com sucesso!");
				doGet(req, resp);
			break;
		case "put":
			DAOFactory.getContatoDAO().update(
					new Contato(
							Integer.parseInt(req.getParameter("id")),
							req.getParameter("nome"),
							req.getParameter("email"),
							req.getParameter("telefone"),
							req.getParameter("fixo"),
							req.getParameter("ramal")
					)
			);
			req.setAttribute("message", "Contato Alterado com sucesso!");
			doGet(req, resp);
		break;
		case "delete":
			DAOFactory.getContatoDAO().delete(Integer.parseInt(req.getParameter("id")));
			req.setAttribute("message", "Contato deletado com sucesso!");
			doGet(req, resp);
		break;
		default:
			req.setAttribute("message", "DEU MERDA!");
			doGet(req, resp);
			System.out.println("DEU MERDA");
			break;
		}
	}

}
