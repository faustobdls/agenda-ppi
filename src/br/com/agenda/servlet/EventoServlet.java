package br.com.agenda.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import br.com.agenda.dao.DAOFactory;
import br.com.agenda.model.Contato;
import br.com.agenda.model.Evento;

/**
 * Servlet implementation class EventoServlet
 */
@WebServlet("/eventos")
public class EventoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EventoServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("aplication/json");
		if(request.getParameter("id") == null || request.getParameter("id").isEmpty()){
			if(request.getParameter("q") == null || request.getParameter("q").isEmpty()){
				if(request.getParameter("list") == null || request.getParameter("list").isEmpty()){
					request.getRequestDispatcher("pages/eventos.jsp").forward(request, response);
				}else{
					Gson gson = new Gson();
					response.getWriter().print(
							gson.toJson(
									DAOFactory.getEventoDAO().loadAll()
							)
					);
				}
			}else{
				Gson gson = new Gson();
				response.getWriter().print(
						gson.toJson(
								DAOFactory.getEventoDAO().filter(
										request.getParameter("q")
								)
						)
				);
				
			}
		}else{
			Gson gson = new Gson();
			response.getWriter().print(
					gson.toJson(
							DAOFactory.getEventoDAO().loadOne(
									Integer.parseInt(
											request.getParameter("id")
									)
							)
					)
			);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		switch (request.getParameter("method")) {
		case "post":
				DAOFactory.getEventoDAO().store(
						new Evento(
								0,
								request.getParameter("nome"),
								request.getParameter("descricao"),
								new java.util.Date(request.getParameter("data"))
						)
				);
				DAOFactory.getEventoDAO().filter(request.getParameter("nome")).get(0);
				System.out.println(request.getParameterValues("participantes"));
				List<Contato> con = new ArrayList<Contato>(); 
				for (String s : request.getParameterValues("participantes")) {
					con.add(DAOFactory.getContatoDAO().loadOne(Integer.parseInt(s)));
				}
				DAOFactory.getEhCDAO().setContatosInEvent(con, DAOFactory.getEventoDAO().filter(request.getParameter("nome")).get(0));
				request.setAttribute("message", "Evento Criado com sucesso!");
				doGet(request, response);
			break;
		case "put":
			DAOFactory.getEventoDAO().update(
					new Evento(
							Integer.parseInt(request.getParameter("idd")),
							request.getParameter("nome"),
							request.getParameter("descricao"),
							new java.util.Date(request.getParameter("data"))
					)
			);
			request.setAttribute("message", "Evento Alterado com sucesso!");
			doGet(request, response);
		break;
		case "delete":
			DAOFactory.getContatoDAO().delete(Integer.parseInt(request.getParameter("idd")));
			System.out.println(request.getParameter("idd"));
			request.setAttribute("message", "Evento deletado com sucesso!");
			doGet(request, response);
		break;
		default:
			request.setAttribute("message", "DEU MERDA!");
			doGet(request, response);
			System.out.println("DEU MERDA");
			break;
		}
	}

}
